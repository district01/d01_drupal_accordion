<?php

namespace Drupal\d01_drupal_accordion\Render;

use Drupal\Core\Render\Element;

/**
 * Class AccordionElement.
 *
 * @package Drupal\d01_drupal_accordion
 */
class AccordionElement extends Element {

  /**
   * Is renderable array.
   *
   * Check if the passed item is a render array.
   *
   * @param mixed $element
   *   An item to check.
   *
   * @return bool
   *   A boolean indicating if item contains #theme, #type or #markup.
   */
  public static function isRenderableArray($element) {

    // Check if we got an array.
    if (!is_array($element)) {
      return FALSE;
    }

    // Check for #theme or #type key.
    if (isset($element['#theme']) || isset($element['#type']) || isset($element['#markup'])) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Is renderable element.
   *
   * Check if the passed item is a render element.
   *
   * @param mixed $element
   *   An item to check.
   *
   * @return bool
   *   A boolean indicating if item is a render element.
   */
  public static function isRenderableElement($element) {
    if (!self::isRenderableArray($element)) {
      return FALSE;
    }

    // Check if it's a render element.
    if (isset($element['#type'])) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Is renderable element of type.
   *
   * Check if the passed item is a render
   * element that is an implementation of
   * the passed $type.
   *
   * @param mixed $element
   *   An item to check.
   * @param string $type
   *   The type to check.
   *
   * @return bool
   *   A boolean indicating if item is of type to check.
   */
  public static function isRenderableElementOfType($element, $type) {
    if (!self::isRenderableArray($element)) {
      return FALSE;
    }

    if (!self::isRenderableElement($element)) {
      return FALSE;
    }

    // Check if it's of correct type.
    if ($element['#type'] === $type) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

}

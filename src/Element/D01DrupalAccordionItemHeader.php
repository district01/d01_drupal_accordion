<?php

namespace Drupal\d01_drupal_accordion\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\d01_drupal_accordion\Render\AccordionElement;

/**
 * An accordion item header render element.
 *
 * @RenderElement("d01_drupal_accordion_item_header")
 */
class D01DrupalAccordionItemHeader extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'd01_drupal_accordion_item_header',
      '#pre_render' => [
        [$class, 'preRenderElement'],
      ],
      '#attributes' => [],
      '#content' => [],
      '#bodyless' => FALSE,
      '#position' => FALSE,
      '#parent_accordion_id' => FALSE,
      '#parent_accordion_type' => 'accordion',
    ];
  }

  /**
   * Prepare the render array for the template.
   */
  public static function preRenderElement($element) {

    // Make sure we only #pre_render item once.
    if (!empty($element['#pre_rendered'])) {
      return $element;
    }

    // We need a render array for our slick slide element
    // but we want to give people full freedom to pass whatever
    // they want to the slide element.So we need to check if we recieve
    // a render array and else we need to convert it to a render array.
    if (!AccordionElement::isRenderableArray($element['#content'])) {
      // Convert string to render array.
      $element['#content'] = ['#markup' => $element['#content']];
    }

    // Add JS trigger classes as attributes,
    // when header is not bodyless.
    if (!$element['#bodyless']) {
      $element['#attributes']['class'][] = 'js-d01-drupal-accordion-header';
    }

    // Convert #header and #content to a renderable elements.
    $element['content'] = $element['#content'];

    // Mark as prerendered.
    $element['#pre_rendered'] = TRUE;
    return $element;
  }

}

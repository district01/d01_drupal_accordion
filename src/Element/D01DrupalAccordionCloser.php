<?php

namespace Drupal\d01_drupal_accordion\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\d01_drupal_accordion\Render\AccordionElement;

/**
 * An accordion closer render element.
 *
 * @RenderElement("d01_drupal_accordion_closer")
 */
class D01DrupalAccordionCloser extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'd01_drupal_accordion_closer',
      '#pre_render' => [
        [$class, 'preRenderElement'],
      ],
      '#attributes' => [],
      '#content' => [],
      '#closer_id' => FALSE,
      '#accordion_ids' => [],
      '#attached' => [
        'library' => [
          'd01_drupal_accordion/accordion',
          'd01_drupal_accordion/accordion_closer',
        ],
        'drupalSettings' => [
          'd01_drupal_accordion_closer' => [],
        ],
      ],
    ];
  }

  /**
   * Prerender function for element.
   */
  public static function preRenderElement($element) {
    // Make sure we only #pre_render item once.
    if (!empty($element['#pre_rendered'])) {
      return $element;
    }

    // We need a #closer_id for theme suggestions and js-settings.
    // This way we can have more than one element on the page with different
    // settings.
    if (!$element['#closer_id']) {
      $element = [
        '#markup' => t('The d01_drupal_accordion_closer element requires a #closer_id to work.'),
      ];
      return $element;
    }

    // We need a render array for our closer content element
    // but we want to give people full freedom to pass whatever
    // they want to the #content property. So we need to check if we receive
    // a render array and else we need to convert it to a render array.
    if (!AccordionElement::isRenderableArray($element['#content'])) {

      // Convert string to render array.
      $element['#content'] = ['#markup' => $element['#content']];
    }

    // Get the #closer_id.
    $js_id = $element['#closer_id'];

    // Set the #closer_id as html Id.
    $element['#attributes']['id'] = $js_id;

    // Add required JS classes.
    $element['#attributes']['class'][] = 'js-d01-drupal-accordion-closer';

    // Pass the settings to js keyed by the #toggler_id.
    $element['#attached']['drupalSettings']['d01_drupal_accordion_closer'][$js_id]['accordion_ids'] = $element['#accordion_ids'];

    // Convert #content to a renderable element.
    $element['content'] = $element['#content'];

    // Mark as prerendered.
    $element['#pre_rendered'] = TRUE;
    return $element;
  }

}

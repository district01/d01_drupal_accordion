<?php

namespace Drupal\d01_drupal_accordion\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\d01_drupal_accordion\Render\AccordionElement;

/**
 * An accordion closer render element.
 *
 * @RenderElement("d01_drupal_accordion_expander")
 */
class D01DrupalAccordionExpander extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'd01_drupal_accordion_expander',
      '#attributes' => [],
      '#pre_render' => [
        [$class, 'preRenderElement'],
      ],
      '#expander_id' => FALSE,
      '#accordion_ids' => [],
      '#content' => [],
      '#attached' => [
        'library' => [
          'd01_drupal_accordion/accordion',
          'd01_drupal_accordion/accordion_expander',
        ],
        'drupalSettings' => [
          'd01_drupal_accordion_expander' => [],
        ],
      ],
    ];
  }

  /**
   * Prerender function for element.
   */
  public static function preRenderElement($element) {
    // Make sure we only #pre_render item once.
    if (!empty($element['#pre_rendered'])) {
      return $element;
    }

    // We need a #expander_id for theme suggestions and js-settings.
    // This way we can have more than one element on the page with different
    // settings.
    if (!$element['#expander_id']) {
      $element = [
        '#markup' => t('The d01_drupal_accordion_expander element requires a #expander_id to work.'),
      ];
      return $element;
    }

    // We need a render array for our expander content element
    // but we want to give people full freedom to pass whatever
    // they want to the #content property. So we need to check if we receive
    // a render array and else we need to convert it to a render array.
    if (!AccordionElement::isRenderableArray($element['#content'])) {
      // Convert string to render array.
      $element['#content'] = ['#markup' => $element['#content']];
    }

    // Convert #content to a renderable element.
    $element['content'] = $element['#content'];

    // Get the #expander_id.
    $js_id = $element['#expander_id'];

    // Set the #expander_id as html Id.
    $element['#attributes']['id'] = $js_id;

    // Add required JS classes.
    $element['#attributes']['class'][] = 'js-d01-drupal-accordion-expander';

    // Pass the settings to js keyed by the #toggler_id.
    $element['#attached']['drupalSettings']['d01_drupal_accordion_expander'][$js_id]['accordion_ids'] = $element['#accordion_ids'];

    // Mark as prerendered.
    $element['#pre_rendered'] = TRUE;
    return $element;
  }

}

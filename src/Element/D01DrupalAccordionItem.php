<?php

namespace Drupal\d01_drupal_accordion\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\d01_drupal_accordion\Render\AccordionElement;

/**
 * An accordion item render element.
 *
 * @RenderElement("d01_drupal_accordion_item")
 */
class D01DrupalAccordionItem extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'd01_drupal_accordion_item',
      '#pre_render' => [
        [$class, 'preRenderElement'],
      ],
      '#header' => [],
      '#body' => [],
      '#position' => FALSE,
      '#parent_accordion_id' => FALSE,
      '#parent_accordion_type' => 'accordion',
    ];
  }

  /**
   * Prepare the render array for the template.
   */
  public static function preRenderElement($element) {
    // Make sure we only #pre_render item once.
    if (!empty($element['#pre_rendered'])) {
      return $element;
    }

    // For an accordion item we at need at least a #header.
    if (empty($element['#header'])) {
      $element['#pre_rendered'] = TRUE;
      return $element;
    }

    // Check if header is of valid #type.
    if (!AccordionElement::isRenderableElementOfType($element['#header'], 'd01_drupal_accordion_item_header')) {
      $element['#pre_rendered'] = TRUE;
      return $element;
    }

    // Pass on variables to header.
    $element['#header']['#position'] = $element['#position'];
    $element['#header']['#parent_accordion_id'] = $element['#parent_accordion_id'];
    $element['#header']['#parent_accordion_type'] = $element['#parent_accordion_type'];

    // When no #body mark the header as bodyless.
    if (empty($element['#body'])) {
      $element['#header']['#bodyless'] = TRUE;
    }

    // Convert #header in render array.
    $element['header'] = $element['#header'];

    // Check if body is empty,
    // when empty we are done so we return the element.
    if (empty($element['#body'])) {
      $element['#pre_rendered'] = TRUE;
      return $element;
    }

    // Check if body is of valid #type,
    // when empty we are done so we return the element.
    if (!AccordionElement::isRenderableElementOfType($element['#body'], 'd01_drupal_accordion_item_body')) {
      $element['#pre_rendered'] = TRUE;
      return $element;
    }

    // Pass on variables to body.
    $element['#body']['#position'] = $element['#position'];
    $element['#body']['#parent_accordion_id'] = $element['#parent_accordion_id'];
    $element['#body']['#parent_accordion_type'] = $element['#parent_accordion_type'];

    // Convert #body to a renderable element.
    $element['body'] = $element['#body'];

    // Mark as prerendered.
    $element['#pre_rendered'] = TRUE;
    return $element;
  }

}

<?php

namespace Drupal\d01_drupal_accordion\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\d01_drupal_accordion\Render\AccordionElement;

/**
 * An accordion render element.
 *
 * @RenderElement("d01_drupal_accordion")
 */
class D01DrupalAccordion extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'd01_drupal_accordion',
      '#accordion_id' => FALSE,
      '#accordion_type' => 'accordion',
      '#js_settings' => [],
      '#attributes' => [],
      '#items' => [],
      '#media_query' => FALSE,
      '#pre_render' => [
        [$class, 'preRenderElement'],
      ],
      '#attached' => [
        'library' => [
          'd01_drupal_accordion/accordion',
        ],
        'drupalSettings' => [
          'd01_drupal_accordion' => [],
        ],
      ],
    ];
  }

  /**
   * Prerender function for element.
   */
  public static function preRenderElement($element) {
    // Make sure we only #pre_render item once.
    if (!empty($element['#pre_rendered'])) {
      return $element;
    }

    // We need a #accordion_id for theme suggestions and js-settings.
    // This way we can have more than one element on the page with different
    // settings.
    if (!$element['#accordion_id']) {
      $element = [
        '#markup' => t('The d01_drupal_accordion element requires a #accordion_id to work.'),
      ];
      return $element;
    }

    // We need to make sure all children added in #items
    // are d01_drupal_accordion_items items. So we remove all
    // other items.
    $count = 0;
    if (!empty($element['#items'])) {
      foreach ($element['#items'] as $key => $child) {
        if (!AccordionElement::isRenderableElementOfType($child, 'd01_drupal_accordion_item')) {
          unset($element['#items'][$key]);
        }
        else {
          // Add the parent accordion_id and the position of the item to
          // the children. This way we can provide more specific
          // theme suggestions.
          //
          $element['#items'][$key]['#position'] = $count;
          $element['#items'][$key]['#parent_accordion_id'] = isset($element['#accordion_id']) ? $element['#accordion_id'] : FALSE;
          $element['#items'][$key]['#parent_accordion_type'] = $element['#accordion_type'];
          $count++;
        }
      }
    }

    // Convert #items to a renderable element.
    $element['items'] = !empty($element['#items']) ? $element['#items'] : [];

    // Since we can't have a accordion without items we
    // return the element which will be an empty array at this point.
    if (!empty($element['items'])) {

      // Get the #accordion_id and the #js_settings.
      $js_id = $element['#accordion_id'];
      $js_settings = is_array($element['#js_settings']) ? $element['#js_settings'] : [];

      // Set the #accordion_id as html Id.
      $element['#attributes']['id'] = $js_id;

      // Add required JS classes.
      $element['#attributes']['class'][] = 'js-d01-drupal-accordion';

      // Pass the settings to js keyed by the #accordion_id.
      $element['#attached']['drupalSettings']['d01_drupal_accordion'][$js_id] = $js_settings;

      if ($element['#media_query']) {
        $element['#attached']['drupalSettings']['d01_drupal_accordion'][$js_id]['mediaQuery'] = $element['#media_query'];
      }
    }

    // Mark as prerendered.
    $element['#pre_rendered'] = TRUE;
    return $element;
  }

}

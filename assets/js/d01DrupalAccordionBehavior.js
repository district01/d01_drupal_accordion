/**
 * d01DrupalAccordion.
 */
(function ($, Drupal) {

  'use strict';

  /**
   * D01 Drupal Accordion behavior.
   * @type {{attach: Drupal.behaviors.d01_drupal_accordion.attach}}
   */
  Drupal.behaviors.d01_drupal_accordion = {
    attach: function (context, drupalSettings) {
      var componentSettings = drupalSettings.d01_drupal_accordion || {};
      var wrapperClass = '.js-d01-drupal-accordion';
      $(wrapperClass, context).once('initializeAccordions').each(function(i, obj) {
        var el = $(this);
        var elId = el.attr('id') ? el.attr('id') : false;
        if (elId) {
          var options = componentSettings[elId] || {};
          new D01DrupalAccordion(
            el,
            '.js-d01-drupal-accordion-header',
            '.js-d01-drupal-accordion-content',
            options
          ).init();
        }
      });

    }
  };
})(jQuery, Drupal);

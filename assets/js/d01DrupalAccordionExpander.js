(function ($) {

  'use strict';

  /**
   * jQuery object
   * @external jQuery
   * @see {@link http://api.jquery.com/jQuery/}
   */

  /**
   * D01DrupalAccordionExpander
   *
   * @param {jQuery} element
   *    a jQuery element.
   * @param {object} settings
   *    an object containing all settings.
   * @constructor
   */
  var D01DrupalAccordionExpander = function (element, settings) {
    var that = this;
    that.element = element;
    that.settings = settings || {};
  };

  /**
   * init().
   */
  D01DrupalAccordionExpander.prototype.init = function () {
    var that = this;
    that.element.on('click', function(e){
      // Prevent default since this element can be any html tag
      // ex : a, div, ...
      e.preventDefault();
      var managed_accordions = that.settings.accordion_ids || [];
      $.each(managed_accordions, function(index, value) {
        var accordion = $('#' + value);
        if (accordion.length > 0) {
          // Accordion tabs aren't meant to be opened all at once.
          // This isn't possible without a hacky implementation.
          // So we opt for 'expander' to open the first item of an
          // accordion. When you want to open all accordions at once
          // every item should be a separate accordion itself.
          accordion.accordion('option', 'active', 0);
        }
      });
    });
  };

  /**
   * @type {D01DrupalAccordionExpander}
   */
  window.D01DrupalAccordionExpander = D01DrupalAccordionExpander;

})(jQuery);

/**
 * D01DrupalAccordionCloserBehavior.
 */
(function ($, Drupal) {

  'use strict';

  /**
   * D01 Drupal Accordion Toggler behavior.
   * @type {{attach: Drupal.behaviors.d01_drupal_accordion_closer.attach}}
   */
  Drupal.behaviors.d01_drupal_accordion_closer = {
    attach: function (context, drupalSettings) {
      var componentSettings = drupalSettings.d01_drupal_accordion_closer || {};
      var wrapperClass = '.js-d01-drupal-accordion-closer';
      $(wrapperClass, context).once('initializeAccordionClosers').each(function(i, obj) {
        var el = $(this);
        var elId = el.attr('id') ? el.attr('id') : false;
        if (elId) {
          var options = componentSettings[elId] || {};
          new D01DrupalAccordionCloser(
            el,
            options
          ).init();
        }
      });
    }
  };
})(jQuery, Drupal);

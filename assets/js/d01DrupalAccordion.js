(function ($) {

  'use strict';

  /**
   * jQuery object
   * @external jQuery
   * @see {@link http://api.jquery.com/jQuery/}
   */

  /**
   * D01DrupalAccordion
   *
   * @param {jQuery} element
   *    a jQuery element.
   * @param {string} headerClass
   *    css class of the accordion item header.
   * @param {string} contentClass
   *    css class of the accordion item content.
   * @param {object} drupalSettings
   *    an object containing all settings.
   * @constructor
   */
  var D01DrupalAccordion = function (element, headerClass, contentClass, settings) {
    var that = this;
    that.element = element;
    that.headerClass = headerClass;
    that.contentClass = contentClass;
    that.settings = settings || {};

    // Store the window width
    that.windowWidth = $(window).width();
    that.isResizing = false;
    that.addEventListeners();
  };

  /**
   * addEventListeners().
   */
  D01DrupalAccordion.prototype.addEventListeners = function () {
    var that = this;
    window.addEventListener('resize', that.resizeEvent.bind(that), false);
  };

  /**
   * resizeEvent().
   */
  D01DrupalAccordion.prototype.resizeEvent = function () {
    var that = this;

    // Check window width has actually changed
    // and it's not just iOS triggering a resize event on scroll
    if ($(window).width() !== that.windowWidth) {
      that.windowWidth = $(window).width();
      that.windowResized();
    }
  };

  /**
   * windowResized().
   */
  D01DrupalAccordion.prototype.windowResized = function () {
    var that = this;

    // Use requestAnimationFrame to debounce resize event.
    if (!that.isResizing) {
      requestAnimationFrame(that.onResize.bind(that));
    }

    that.isResizing = true;
  };

  /**
   * onResize().
   */
  D01DrupalAccordion.prototype.onResize = function () {
    var that = this;
    that.isResizing = false;
    that.init();
  };

  /**
   * initializeAccordions().
   */
  D01DrupalAccordion.prototype.init = function () {
    var that = this;

    var accordionId = that.element.attr('id');
    var accordionIsEnabled = that.element.hasClass('ui-accordion');

    that.settings['header'] = that.headerClass;

    if (that.accordionShouldBeActive(accordionId)) {
      if (!accordionIsEnabled) {

        // Enable accordions that aren't active but should be.
        $('#' + accordionId).accordion(that.settings);
      }
    }
    else {
      if (accordionIsEnabled) {

        // Destroy active accordions that should not be active.
        that.element.accordion('destroy');
      }
    }
  };

  /**
   * accordionShouldBeActive().
   *
   * @param {string} accordionId
   *    the accordion id.
   *
   * @return {boolean}
   *    boolean indicating if accordion should be activated.
   */
  D01DrupalAccordion.prototype.accordionShouldBeActive = function (accordionId) {
    var that = this;
    var accordionMediaQuery = that.settings.mediaQuery || false;
    return accordionMediaQuery ? window.matchMedia(accordionMediaQuery).matches : true;
  };

  /**
   * @type {D01DrupalAccordion}
   */
  window.D01DrupalAccordion = D01DrupalAccordion;

})(jQuery);

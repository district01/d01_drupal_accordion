(function ($) {

  'use strict';

  /**
   * jQuery object
   * @external jQuery
   * @see {@link http://api.jquery.com/jQuery/}
   */

  /**
   * D01DrupalAccordionCloser
   *
   * @param {jQuery} element
   *    a jQuery element.
   * @param {object} settings
   *    an object containing all settings.
   * @constructor
   */
  var D01DrupalAccordionCloser = function (element, settings) {
    var that = this;
    that.element = element;
    that.settings = settings || {};
  };

  /**
   * init().
   */
  D01DrupalAccordionCloser.prototype.init = function () {
    var that = this;
    that.element.on('click', function(e) {
      // Prevent default since this element can be any html tag
      // ex : a, div, ...
      e.preventDefault();
      var managed_accordions = that.settings.accordion_ids || [];
      $.each(managed_accordions, function(index, value) {
        var accordion = $('#' + value);
        if (accordion.length > 0) {
          accordion.accordion('option', 'active', false);
        }
      });
    });
  };

  /**
   * @type {D01DrupalAccordionCloser}
   */
  window.D01DrupalAccordionCloser = D01DrupalAccordionCloser;

})(jQuery);

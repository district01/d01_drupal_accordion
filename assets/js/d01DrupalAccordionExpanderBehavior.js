/**
 * D01DrupalAccordionExpanderBehavior.
 */
(function ($, Drupal) {

  'use strict';

  /**
   * D01 Drupal Accordion Expander behavior.
   * @type {{attach: Drupal.behaviors.d01_drupal_accordion_expander.attach}}
   */
  Drupal.behaviors.d01_drupal_accordion_expander = {
    attach: function (context, drupalSettings) {
      var componentSettings = drupalSettings.d01_drupal_accordion_expander || {};
      var wrapperClass = '.js-d01-drupal-accordion-expander';
      $(wrapperClass, context).once('initializeAccordionExpanders').each(function(i, obj) {
        var el = $(this);
        var elId = el.attr('id') ? el.attr('id') : false;
        if (elId) {
          var options = componentSettings[elId] || {};
          new D01DrupalAccordionExpander(
            el,
            options
          ).init();
        }
      });
    }
  };
})(jQuery, Drupal);

# d01_drupal_accordion #

Provides a fully themeable drupal implementation of the JqueryUI accordion.

# Usage #

Build the render array for an accordion.

```
  // Accordion.
  $variables['accordion'] = [
    '#type' => 'd01_drupal_accordion',
    '#accordion_type' => 'MY_ACCORDION_TYPE', // For theme suggestions.
    '#accordion_id' => 'MY_UNIQUE_ID',
    '#attributes' => [
      'class' => [
        'm-accordion',
      ],
    ],
    '#js_settings' => [
      'active' => FALSE,
      'animate' => 250,
      'collapsible' => TRUE,
      'heightStyle' => 'content',
    ],
  ];

  foreach ($items as $item) {

    // Build an accordion item.
    $variables['accordion']['#items'][] =
      '#type' => 'd01_drupal_accordion_item',
      '#attributes' => [
        'class' => [
          'm-accordion-item',
        ],
      ],
      '#header' => [
        '#type' => 'd01_drupal_accordion_item_header',
        '#content' => t('Header'), // Can be string or render array.
        '#attributes' => [
          'class' => [
            'm-accordion-item__header',
          ],
        ],
      ],
      '#body' => [
        '#type' => 'd01_drupal_accordion_item_body',
        '#content' => t('Body'), // Can be string or render array.
        '#attributes' => [
          'class' => [
            'm-accordion-item__body,
          ],
        ],
      ],
    ];
  }
```

# Release notes #

`1.0`
* Basic setup of element.

`2.0`
* Add support for bodyless headers in accordion.

`3.0`
* Add media query support for accordion.

`4.0`
* Fix caching issues with max-age.

`5.0`
* Move initialization to behaviors.
* Provide helper AccordionElement for recurring functions.
* Add a closer and expander element.
* Fix jQuery once dependency issue.

`5.0.2`
* Fix issue on resizing of accordions.

`6.0.0`
Make module Drupal 9 compatible.

:warning: As of this version, this module has a dependency on the "jQuery UI Accordion"-module!

### Manual actions

* Run `drush updb`.
* If there are occurrences like the following in your custom theme/modules:

```
  stylesheets-remove:
    - core/assets/vendor/jquery.ui/themes/base/core.css
    - core/assets/vendor/jquery.ui/themes/base/accordion.css
```
Replace it by the following:
```
  stylesheets-remove:
    - '@jquery_ui_accordion/jquery.ui/themes/base/accordion.css'
    - '@jquery_ui/jquery.ui/themes/base/core.css'
```